#include <sys/cdefs.h>
#include <lib.h>
#include "namespace.h"

#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <andy_time.h>

void andy_time(int * sec, int * nsec){
    
     
    struct timespec time; 
    clock_gettime(time);
    sec = timer->tv_sec;
    nsec = timer->tv_nsec;


    memset(&m, 0, sizeof(m));
   
    if (_syscall(PM_PROC_NR, PM_CLOCK_GETTIME, &m) < 0)
  	    return -1;

    sec->tv_sec = m.m_pm_lc_time.sec;
    nsec->tv_nsec = m.m_pm_lc_time.nsec;
 
  return 0;
};
